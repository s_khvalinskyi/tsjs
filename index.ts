interface Person {
    firstName: string;
    lastName: string;
    nickName?: string;
}

class Student {
    constructor(public firstName: string, private middleInitial: string, public lastName: string) {
        this.fullName = `${this.firstName} ${this.middleInitial} ${this.lastName}`;
    }
    public fullName: string;

}

function greeter(person: Person): string {
    return `Hello, ${person.firstName} ${person.lastName} ${ person.nickName ? '"' + person.nickName + '"' : '' }`;
}

const user = new Student('Jane', 'M.', 'Doe');

console.log(greeter(user));
user['middleInitial'] = 'S.'
console.log(user['middleInitial']);
user['nickName'] = 'Doll';
console.log(greeter(user));