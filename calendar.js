const currentDate = new Date();
/**
 * @param {Number} month 
 * @param {Number} year 
 * @returns {string} calendar for the month
 */
function showCalendar(month = currentDate.getMonth() + 1, year = currentDate.getFullYear()) {
    const daysInMonth = new Date(year, month, 0).getDate();
    const dayOfWeek = new Date(year, month - 1).getDay();
    const line = '-'.repeat(20).concat('\n');
    const header = 'SU MO TU WE TH FR SA\n';
    //return line.concat(header).concat(line);
    let calendarBody = new Array(dayOfWeek).fill('  ');
    for (let n = 0; n < daysInMonth; n++) {
        calendarBody = calendarBody.concat([`${(n+1).toLocaleString('en-US', {minimumIntegerDigits: 2})}`]);
        if (!((n + 1 + dayOfWeek) % 7)) {
            calendarBody = calendarBody.concat('\n');
        }
    }
    return line.concat(header).concat(line).concat(calendarBody.join(' ')).replace(/\s\n\s/g, '\n');
}

console.log(showCalendar(6, 2019));